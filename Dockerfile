FROM debian:buster
RUN echo deb http://deb.debian.org/debian buster-backports main | tee /etc/apt/sources.list.d/buster-backports.list \
	&& apt-get update \
	&& apt install -y -t buster-backports apache2 \
	&& apt-get install -y ruby2.5 bundler \
	&& apt-get clean
ARG INGRESS_HOME=/var/lib/ingress
RUN mkdir -p $INGRESS_HOME
WORKDIR $INGRESS_HOME
COPY Gemfile Gemfile.lock ./
RUN bundle install --deployment --no-cache --without development
COPY ingress/* ./
ENTRYPOINT ["bundle", "exec", "./ingress.rb"]
