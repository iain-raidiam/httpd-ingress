#!/usr/bin/env ruby

require 'base64'
require 'erb'
require 'kubeclient'
require 'thread'

@conf_dir = 'httpd'
@ingress_class = ENV['INGRESS_CLASS'] || 'httpd'

@http_port = ENV['HTTP_PORT'] || 80
@https_port = ENV['HTTPS_PORT'] || 443

HostConfig = Struct.new(:paths, :snippets)

@config_lock = Mutex.new

@httpd_lock = Mutex.new
@httpd_needs_restart = false
@httpd_poke = ConditionVariable.new

@ingresses = []
@secrets = Hash.new

def filter_by_ingress_class(ingresses)
    ingresses.select do |i|
        ann = i.metadata.annotations
        icls = ann && ann['kubernetes.io/ingress.class']
        icls && icls == @ingress_class
    end
end

def fetch_secrets(client, ingresses)
    puts 'Fetching secrets'
    secret_path = File.join(@conf_dir, 'ssl')
    our_ingresses = filter_by_ingress_class(ingresses)
    Dir.mkdir secret_path unless Dir.exist?(secret_path)
    secrets = ingresses.inject(Hash.new) do |secrets, ingress|
        (ingress.spec[:tls] || []).inject(secrets) do |secrets, tls|
            secret_name = tls.secretName
            crt_file = File.join(secret_path, "#{secret_name}.crt")
            key_file = File.join(secret_path, "#{secret_name}.key")
            begin # Refetch all secrets in case any have changed
                secret =
                  begin
                    client.get_secret(secret_name, ingress.metadata.namespace)
                  rescue KubeException
                    puts "Secret not found: #{secret_name}"
                    next secrets
                  end
                if secret.data['tls.crt'].empty?
                  puts "tls.crt is empty for secret: #{secret_name}"
                  next secrets
                end
                File.open(crt_file, 'w') {|f| f.write Base64.decode64(secret.data['tls.crt'])}
                File.open(key_file, 'w') {|f| f.write Base64.decode64(secret.data['tls.key'])}
            end
            tls.hosts.each {|host| secrets[host] = {
                cert_path: "ssl/#{secret_name}.crt",
                key_path: "ssl/#{secret_name}.key",
                metadata: secret.metadata
            }}
            secrets
        end
    end
end

def update_config(client, ext_client)
    old_ingresses = @ingresses
    old_secrets = @secrets

    puts 'Updating config'
    @ingresses = filter_by_ingress_class(ext_client.get_ingresses)
    @ingresses.sort_by! {|i| i.metadata[:name]}
    @secrets = fetch_secrets(client, @ingresses)

    if @ingresses.map(&:metadata) != old_ingresses.map(&:metadata) || @secrets != old_secrets
        write_config @ingresses, @secrets
        true
    else
        puts 'No changes to config'
        false
    end
end

def write_config(ingresses, secrets)
    puts 'Writing config'
    hosts = ingresses.inject(Hash.new) do |hosts, ingress|
        rules = ingress.spec.rules
        rules.inject(hosts) do |hosts, rule|
            ann = ingress.metadata.annotations
            snippet = ann && ann['httpd-server-snippet']
            acme_enabled = (ann && ann['kubernetes.io/tls-acme']) || 'false'
            ssl_redirect = (ann && ann['ingress.kubernetes.io/ssl-redirect']) || acme_enabled

            paths = rule.http.paths.map do |path|
                backendHost = "#{path.backend.service.name}.#{ingress.metadata.namespace}.svc.cluster.local"
                [ path.path, "http://#{backendHost}:#{path.backend.service.port.number}", (ssl_redirect == 'true') ]
            end

            host = (hosts[rule.host] ||= HostConfig.new([], []))
            host.paths.concat paths
            host.paths.sort_by! {|path, backend| -path.length}
            host.snippets << snippet if snippet
            hosts
        end
    end
    filename = File.join(File.dirname(__FILE__), 'httpd.conf.erb')
    erb = ERB.new(File.read(filename))
    erb.filename = filename
    bind = binding
    File.open(File.join(@conf_dir, 'httpd.conf'), 'w') {|f| f.write erb.result(bind)}
end

(kube_client, kube_ext_client) =
    if kube_url = ENV['KUBE_URL']
        kube_ext_url = kube_url.gsub(%r{/api/?$}, '/apis/networking.k8s.io/')
        [Kubeclient::Client.new(kube_url, 'v1'),
         Kubeclient::Client.new(kube_ext_url, 'v1')]
    else
        # Try k8s secret
        auth_options = {
            bearer_token_file: '/var/run/secrets/kubernetes.io/serviceaccount/token'
        }
        ssl_options = {
            verify_ssl: OpenSSL::SSL::VERIFY_NONE
        }
        [Kubeclient::Client.new('https://kubernetes/api/', 'v1', auth_options: auth_options, ssl_options: ssl_options),
         Kubeclient::Client.new('https://kubernetes/apis/networking.k8s.io/', 'v1', auth_options: auth_options, ssl_options: ssl_options)]
    end

def signal_restart_httpd
    @httpd_lock.synchronize {
        @httpd_needs_restart = true
        @httpd_poke.signal
    }
end

Dir.mkdir @conf_dir unless Dir.exist?(@conf_dir)

# Write initial config
update_config kube_client, kube_ext_client

Thread.abort_on_exception = true # We'd rather the container be restarted

# Start the ingress watcher
ingress_watcher = Thread.new do
    loop do
        kube_ext_client.watch_ingresses.each do |notice|
            obj = RecursiveOpenStruct.new(notice.object.to_h, recurse_over_arrays: true)
            puts "Ingress update: #{obj.metadata.name}"
            signal_restart_httpd
        end
        puts 'Ingress watcher stopped! Restarting...'
    end
end

# Start the secret watcher
secret_watcher = Thread.new do
    loop do
        kube_client.watch_secrets.each do |notice|
            obj = RecursiveOpenStruct.new(notice.object.to_h, recurse_over_arrays: true)
            ann = obj.metadata['annotations']
            next unless ann && ann['cert-manager.io/common-name']
            puts "TLS secret update: #{obj.metadata.name}"
            signal_restart_httpd
        end
        puts 'Secret watcher stopped! Restarting...'
    end
end

httpd_restarter = Thread.new do
    loop do
        @httpd_lock.synchronize {
            while !@httpd_needs_restart
                @httpd_poke.wait(@httpd_lock)
            end
        }

        # Sleep for a while in case there are multiple updates to process
        sleep 5
        @httpd_lock.synchronize {
            @httpd_needs_restart = false
        }

        # Rewrite the config and restart httpd if necessary
        next unless @config_lock.synchronize {
            update_config kube_client, kube_ext_client
        }

        puts 'Checking config'
        config_ok = system '/usr/sbin/apachectl', '-d', @conf_dir, '-f', 'httpd.conf', '-t'
        if config_ok
            puts 'Restarting httpd'
            system '/usr/sbin/apachectl', '-d', @conf_dir, '-f', 'httpd.conf', '-k', 'graceful'
        else
            puts 'Invalid configuration; not restarting'
        end

        # Give httpd a few seconds at least to recover before the next restart
        sleep 5
    end

    # Not expecting to get here, but just in case...
    fail 'HTTPD restarter died!'
end

# Don't want to fork here or we'll lose the updater thread

result = system '/usr/sbin/apachectl', '-d', @conf_dir, '-f', 'httpd.conf', '-e', 'info', '-DFOREGROUND'

fail 'Unable to launch httpd' if result.nil?

fail 'httpd terminated!'
